package com.example.smail.security;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.thymeleaf.exceptions.TemplateProcessingException;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@WithMockUser(roles = "MANAGER")
@WithUserDetails("man")
public class SecurityTestsManager {

	@Autowired
	private WebApplicationContext context;

	private MockMvc mvc;

	@Before
	public void setup() {
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}

	// */
	@Test
	public void home() throws Exception {
		mvc.perform(get("/").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk());
	}

	// *signin
	@Test
	public void signin() throws Exception {
		mvc.perform(get("/signin").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk());
	}

	// *signup
	@Test
	public void signup() throws Exception {
		mvc.perform(get("/signup").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk());
	}

	// *
	// *order/total
	@Test
	public void order_total() throws Exception {
		mvc.perform(post("/order/total").with(csrf()).contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk());
	}

	// *order/add
	@Test
	public void order_add() throws Exception {
		mvc.perform(get("/order/add").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk());
	}

	// *order/list
	@Test
	public void order_list() throws Exception {
		mvc.perform(get("/order/list").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk());
	}
	// *order/save
	@Test
	public void order_save() throws Exception {
		mvc.perform(post("/order/save").with(csrf()).contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk());
	}

	// *order/edit
	@Test
	public void order_edit() throws Exception {
		mvc.perform(get("/order/edit?id=-1").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk());
	}
	// *order/copy
	@Test
	public void order_copy() throws Exception {
		mvc.perform(get("/order/copy?id=-1").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk());
	}
	// *order/delete
	@Test
	public void order_delete() throws Exception {
		mvc.perform(get("/order/delete?id=-1").contentType(MediaType.TEXT_HTML))
				.andExpect(status().is5xxServerError());
	}

	// *invoice/list
	@Test
	public void invoice_list() throws Exception {
		mvc.perform(get("/invoice/list").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk());
	}
	// *invoice/pay
	@Test
	public void invoice_pay() throws Exception {
		mvc.perform(get("/invoice/pay").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isForbidden());
	}

	// *invoice/add
	@Test
	public void invoice_add() throws Exception {
		mvc.perform(get("/invoice/add?order_id=-1").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk());
	}

//	// *invoice/save
//	@Test
//	public void invoice_save() throws Exception {
//		mvc.perform(post("/invoice/save").with(csrf()).contentType(MediaType.TEXT_HTML))
//				.andExpect(status().isOk());
//	}

	// *invoice/edit
	@Test
	public void invoice_edit() throws Exception {
		mvc.perform(get("/invoice/edit?id=-1").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk());
	}

	// *invoice/delete
	@Test
	public void invoice_delete() throws Exception {
		mvc.perform(get("/invoice/delete?id=-1").contentType(MediaType.TEXT_HTML))
				.andExpect(status().is5xxServerError());
	}


	// *	distance/list
	@Test
	public void distance_list() throws Exception {
		mvc.perform(get("/distance/list").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk());
	}
	// *	distance/add
	@Test
	public void distance_add() throws Exception {
		mvc.perform(get("/distance/add").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk());
	}
	// *	distance/delete
	// *	distance/save

	// *	report/order
	@Test
	public void report_order() throws Exception {
		mvc.perform(get("/report/order").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk());
	}

}