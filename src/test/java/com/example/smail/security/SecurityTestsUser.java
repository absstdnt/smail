package com.example.smail.security;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@WithMockUser(roles = "USER")
public class SecurityTestsUser {

	@Autowired
	private WebApplicationContext context;

	private MockMvc mvc;

	@Before
	public void setup() {
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}

	// */
	@Test
	public void home() throws Exception {
		mvc.perform(get("/").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk());
	}

	// *signin
	@Test
	public void signin() throws Exception {
		mvc.perform(get("/signin").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk());
	}

	// *signup
	@Test
	public void signup() throws Exception {
		mvc.perform(get("/signup").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk());
	}

	// *
	// *order/total
	@Test
	public void order_total() throws Exception {
		mvc.perform(post("/order/total").with(csrf()).contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk());
	}

	// *order/add
	@Test
	public void order_add() throws Exception {
		mvc.perform(get("/order/add").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk());
	}

	// *order/list
	@Test
	public void order_list() throws Exception {
		mvc.perform(get("/order/list").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isForbidden());
	}
	// *order/save
	@Test
	public void order_save() throws Exception {
		mvc.perform(post("/order/save").with(csrf()).contentType(MediaType.TEXT_HTML))
				.andExpect(status().isForbidden());
	}

	// *order/edit
	@Test
	public void order_edit() throws Exception {
		mvc.perform(get("/order/edit").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isForbidden());
	}
	// *order/copy
	@Test
	public void order_copy() throws Exception {
		mvc.perform(get("/order/copy").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isForbidden());
	}
	// *order/delete
	@Test
	public void order_delete() throws Exception {
		mvc.perform(get("/order/delete").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isForbidden());
	}

	// *invoice/list
	@Test
	public void invoice_list() throws Exception {
		mvc.perform(get("/invoice/list").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isForbidden());
	}
	// *invoice/pay
	// *invoice/add
	// *invoice/save
	// *invoice/edit
	// *invoice/delete

	// *	distance/list
	@Test
	public void distance_list() throws Exception {
		mvc.perform(get("/distance/list").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk());
	}
	// *	distance/add
	@Test
	public void distance_add() throws Exception {
		mvc.perform(get("/distance/add").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isForbidden());
	}
	// *	distance/delete
	// *	distance/save

	// *	report/order
	@Test
	public void report_order() throws Exception {
		mvc.perform(get("/report/order").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isForbidden());
	}

}