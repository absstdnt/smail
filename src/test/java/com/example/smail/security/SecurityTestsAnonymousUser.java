package com.example.smail.security;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

/**
 * Endpoints
 *	/
 *	signin
 *	signup
 *
 *	order/total
 *	order/add
 *	order/list
 *	order/save
 *	order/edit
 *	order/copy
 *	order/delete
 *
 *	invoice/list
 *	invoice/pay
 *	invoice/add
 *	invoice/save
 *	invoice/edit
 *	invoice/delete
 *
 *	distance/list
 *	distance/add
 *	distance/delete
 *	distance/save
 *
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@WithAnonymousUser
public class SecurityTestsAnonymousUser {

	@Autowired
	private WebApplicationContext context;

	private MockMvc mvc;

	@Before
	public void setup() {
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}

	// */
	@Test
	public void home() throws Exception {
		mvc.perform(get("/").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk());
	}

	// *signin
	@Test
	public void signin() throws Exception {
		mvc.perform(get("/signin").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk());
	}

	// *signup
	@Test
	public void signup() throws Exception {
		mvc.perform(get("/signup").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk());
	}

	// *
	// *order/total
	@Test
	public void order_total() throws Exception {
		mvc.perform(get("/order/total").contentType(MediaType.TEXT_HTML))
				.andExpect(status().is3xxRedirection());
	}

	// *order/add
	@Test
	public void order_add() throws Exception {
		mvc.perform(get("/order/add").contentType(MediaType.TEXT_HTML))
				.andExpect(status().is3xxRedirection());
	}

	// *order/list
	@Test
	public void order_list() throws Exception {
		mvc.perform(get("/order/list").contentType(MediaType.TEXT_HTML))
				.andExpect(status().is3xxRedirection());
	}
	// *order/save
	@Test
	public void order_save() throws Exception {
		mvc.perform(get("/order/save").contentType(MediaType.TEXT_HTML))
				.andExpect(status().is3xxRedirection());
	}

	// *order/edit
	@Test
	public void order_edit() throws Exception {
		mvc.perform(get("/order/edit").contentType(MediaType.TEXT_HTML))
				.andExpect(status().is3xxRedirection());
	}
	// *order/copy
	@Test
	public void order_copy() throws Exception {
		mvc.perform(get("/order/copy").contentType(MediaType.TEXT_HTML))
				.andExpect(status().is3xxRedirection());
	}
	// *order/delete
	@Test
	public void order_delete() throws Exception {
		mvc.perform(get("/order/delete").contentType(MediaType.TEXT_HTML))
				.andExpect(status().is3xxRedirection());
	}

	// *invoice/list
	@Test
	public void invoice_list() throws Exception {
		mvc.perform(get("/invoice/list").contentType(MediaType.TEXT_HTML))
				.andExpect(status().is3xxRedirection());
	}
	// *invoice/pay
	// *invoice/add
	// *invoice/save
	// *invoice/edit
	// *invoice/delete

	// *	distance/list
	@Test
	public void distance_list() throws Exception {
		mvc.perform(get("/distance/list").contentType(MediaType.TEXT_HTML))
				.andExpect(status().is3xxRedirection());
	}
	// *	distance/add
	@Test
	public void distance_add() throws Exception {
		mvc.perform(get("/distance/add").contentType(MediaType.TEXT_HTML))
				.andExpect(status().is3xxRedirection());
	}
	// *	distance/delete


	// *	report/order
	@Test
	public void report_order() throws Exception {
		mvc.perform(get("/report/order").contentType(MediaType.TEXT_HTML))
				.andExpect(status().is3xxRedirection());
	}

}