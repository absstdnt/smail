package com.example.smail.orderType;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author user
 */


@RunWith(SpringRunner.class)
public class OrderTypeServiceMockTest {

    @TestConfiguration
    public static class OrderTypeServiceTestContextConfiguration {

        @Bean
        public OrderTypeService orderTypeService() {
            return new OrderTypeService();
        }
    }

    @Autowired
    OrderTypeService orderTypeService;

    @MockBean
    private OrderTypeRepository orderTypeRepository;

    @Before
    public void setUp() {
        OrderType ot01 = new OrderType(1, "Order type 01");
        Mockito.when(orderTypeRepository.findById(1))
                .thenReturn(java.util.Optional.of(ot01));
    }

    @Test
    public void whenValidID_thenOrderTypeShouldBeFound() {
        int id = 1;
        OrderType found = orderTypeService.findById(id);
        assertEquals(found.getId(), id);
    }

}