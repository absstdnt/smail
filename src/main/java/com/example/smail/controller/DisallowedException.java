package com.example.smail.controller;

import com.example.smail.account.AccountDetails;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author user
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Bad request")
public class DisallowedException extends AllowedException {

    AccountDetails accountDetails;

    public DisallowedException() {
        super();
    }

    public DisallowedException(String message) {
        super(message);
    }
    public DisallowedException(String message, AccountDetails accountDetails) {
        super(message);
        this.accountDetails = accountDetails;
    }

    public DisallowedException(String message, Throwable cause) {
        super(message, cause);
    }
}