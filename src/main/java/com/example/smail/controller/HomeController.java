package com.example.smail.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Slf4j
@Controller
public class HomeController {
	@GetMapping("/")
	String home(Principal principal) {
		return principal != null ? "homeSignedIn" : "homeNotSignedIn";
	}
}
