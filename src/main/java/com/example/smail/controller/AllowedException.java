package com.example.smail.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author user
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Bad request")
public class AllowedException extends RuntimeException {

    public AllowedException() {
        super();
    }

    public AllowedException(String message) {
        super(message);
    }

    public AllowedException(String message, Throwable cause) {
        super(message, cause);
    }
}