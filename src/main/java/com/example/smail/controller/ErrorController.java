package com.example.smail.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author user
 */
@Slf4j
@ControllerAdvice
public class ErrorController {

    private Throwable throwable;
    private Model model;

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String exception(final Throwable throwable, final Model model) {
        this.throwable = throwable;
        this.model = model;
        log.error("Exception during execution of application", throwable);
        return "warning"; //not "error", not to scare user
    }

    @ExceptionHandler(AllowedException.class)
    public String allowedException(final Throwable throwable, final Model model) {
        this.throwable = throwable;
        this.model = model;
        log.error("Allowed exception during execution of application", throwable);
        String description = (throwable != null ? throwable.getMessage() : "Nothing to say...");
        model.addAttribute("description", description);
        return "warning";
    }
}