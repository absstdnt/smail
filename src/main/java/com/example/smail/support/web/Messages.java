package com.example.smail.support.web;

/**
 * @author user
 */
public interface Messages {
    String NOT_BLANK_MESSAGE = "{notBlank.message}";
    String FUTURE_DATE = "{futureDate.message}";
    String GREATER_ZERO_MESSAGE = "{greaterZero.message}";

}
