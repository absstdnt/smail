package com.example.smail.support.web;

import org.springframework.data.domain.Sort;

import java.util.stream.Collectors;

/**
 * @author user
 */
public final class SortUtils {

    private SortUtils(){};

    public static String getSortURLparam(Sort sort) {
        return sort.map(o -> "'" + o.getProperty() + "," + o.getDirection() + "'")
                .stream().collect(Collectors.joining(", sort=", ", sort=", ""));
    }

    public static String getSortURL(Sort sort) {
        return sort.map(o -> "'" + o.getProperty() + "," + o.getDirection() + "'")
                .stream().collect(Collectors.joining("&sort=", "&sort=", ""));
    }

}
