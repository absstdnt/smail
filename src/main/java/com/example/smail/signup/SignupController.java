package com.example.smail.signup;

//import com.example.smail.service.UserService;
import com.example.smail.account.Account;
import com.example.smail.account.AccountDetails;
import com.example.smail.account.AccountService;
import com.example.smail.support.web.MessageHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
//import com.example.smail.support.account.Account;
//import com.example.smail.support.account.AccountService;
//import com.example.smail.support.support.web.MessageHelper;
import com.example.smail.support.web.AjaxUtils;

import javax.validation.Valid;

@Slf4j
@Controller
class SignupController {

	private static final String SIGNUP_VIEW_NAME = "signup";

//	@Autowired
//	private UserService userService;
	@Autowired
	private AccountService accauntService;

	@GetMapping("signup")
	String signup(Model model, @RequestHeader(value = "X-Requested-With", required = false) String requestedWith) {
		model.addAttribute(new SignupForm());
		if (AjaxUtils.isAjaxRequest(requestedWith)) {
			return SIGNUP_VIEW_NAME.concat(" :: signupForm");
		}
		return SIGNUP_VIEW_NAME;
	}

	@PostMapping("signup")
	public String signup(@Valid @ModelAttribute SignupForm signupForm, Errors errors, RedirectAttributes ra) {
		if (errors.hasErrors()) {
			return SIGNUP_VIEW_NAME;
		}
//		UserDetails user = null;
//		try {
//			user = userService.save(signupForm.getEmail(), signupForm.getPassword());
//		} catch (Exception ex) {
//			log.debug(ex.getMessage());
//			MessageHelper.addErrorAttribute(ra, "signup.error", signupForm.getEmail());
//			return "redirect:/"+SIGNUP_VIEW_NAME;
//		}
//		userService.signin(user);

		Account account;

		try {
			account = accauntService.save(signupForm.getEmail(), signupForm.getPassword());
		} catch (Exception ex) {
			log.debug(ex.getMessage());
			MessageHelper.addErrorAttribute(ra, "signup.error", signupForm.getEmail());
			return "redirect:/"+SIGNUP_VIEW_NAME;
		}

		AccountDetails.signin(account);

        MessageHelper.addSuccessAttribute(ra, "signup.success");
		return "redirect:/";
	}
}
