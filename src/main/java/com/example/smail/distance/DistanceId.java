package com.example.smail.distance;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author user
 */

@NoArgsConstructor
@AllArgsConstructor
public class DistanceId implements Serializable {

    int from;
    int to;

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof DistanceId)) return false;
        DistanceId d = (DistanceId) obj;
        return from == d.from && to == d.to;
    }

    @Override
    public int hashCode() {
        return from * 31 + to;
    }

}