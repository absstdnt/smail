package com.example.smail.distance;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * @author user
 */
@Repository
public interface DistanceRepository extends JpaRepository<Distance, DistanceId> {
    @Query("select d from #{#entityName} d where" +
            " (:from_id = -1 or d.from.id = :from_id)" +
            " and (:to_filter = '' or LOWER(to.name) like concat(LOWER(?#{escape([2])}), '%') escape ?#{escapeCharacter()})")
    Page<Distance> findAll(Pageable pageable, Integer from_id, String to_filter);
}
