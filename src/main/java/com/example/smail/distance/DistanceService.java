package com.example.smail.distance;

import com.example.smail.controller.AllowedException;
import com.example.smail.destination.Destination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author user
 */
@Service
public class DistanceService {

    @Autowired
    private DistanceRepository distanceRepository;

    public Page<Distance> findAll(Pageable pageable) {
        return distanceRepository.findAll(pageable);
    }

    public void save(Distance distance) {
        distanceRepository.save(distance);
    }

    public void deleteById(int from_id, int to_id) {
        distanceRepository.deleteById(new DistanceId(from_id, to_id));
    }

    public Page<Distance> findAll(Pageable pageable, Optional<Integer> from_id, Optional<String> to_filter) {
            return distanceRepository.findAll(pageable, from_id.orElse(-1), to_filter.orElse(""));
    }

    public int getDistance(int from_id, int to_id) {
        Distance distance = distanceRepository.findById(new DistanceId(from_id, to_id))
                .orElseThrow(() -> new AllowedException(String.format("Could not find distance between IDs %s and %s", from_id, to_id)));
        return distance.getValue();
    }

    public int getDistance(Destination from, Destination to) {
        try {
            return getDistance(from.getId(), to.getId());
        } catch (Exception ex){
            throw new AllowedException(String.format("Could not find distance between %s and %s", from, to), ex);
        }
    }
}
