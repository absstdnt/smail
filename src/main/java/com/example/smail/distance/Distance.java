package com.example.smail.distance;

import com.example.smail.destination.Destination;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author user
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@IdClass(DistanceId.class)
public class Distance implements Serializable {
    @Id
    @ManyToOne
    @JoinColumn(name = "from_id")
    @NotNull(message = "Field From cannot be empty")
    Destination from;

    @Id
    @ManyToOne
    @JoinColumn(name = "to_id")
    @NotNull(message = "Field To cannot be empty")
    Destination to;

    int value;

    @Override
    public String toString() {
        return "Distance{" +
                "from=" + from +
                ", to=" + to +
                ", value=" + value +
                '}';
    }
}