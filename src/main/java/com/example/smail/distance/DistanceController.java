package com.example.smail.distance;

import com.example.smail.destination.DestinationService;
import com.example.smail.support.web.SortUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

/**
 * @author user
 */
@Slf4j
@Controller
@RequestMapping("/distance")
public class DistanceController {

    @Autowired
    private DestinationService destinationService;

    @Autowired
    private DistanceService distanceService;

    @GetMapping("/list")
    public String list(@PageableDefault(sort = "from") Pageable pageable,
                       @RequestParam Optional<Integer> from_id,
                       @RequestParam Optional<String> to_filter,
                       Model model
    ) {
        model.addAttribute("from_id", from_id.orElse(-1));
        model.addAttribute("to_filter", to_filter.orElse(""));
        model.addAttribute("pageable", pageable);
        model.addAttribute("destinations", destinationService.findAll());
        Page<Distance> page = distanceService.findAll(pageable, from_id, to_filter);
        model.addAttribute("distances", page);
        model.addAttribute("sortURLparams", SortUtils.getSortURLparam(page.getSort()));

        return "distance-list";
    }


    @GetMapping("/add")
    public String add(@RequestParam(value = "from_id", defaultValue = "-1") int from_id,
                      @RequestParam(value = "to_id", defaultValue = "-1") int to_id,
                      @RequestParam(value = "value", defaultValue = "0") int value,
                      Model model) {
        Distance distance = new Distance();
        if (from_id > 0) distance.setFrom(destinationService.findById(from_id));
        if (to_id > 0) distance.setTo(destinationService.findById(to_id));
        distance.setValue(value);
        model.addAttribute("distance", distance);
        model.addAttribute("destinations", destinationService.findAll());
        return "distance-form";
    }

    @GetMapping("/delete")
    public String delete(@RequestParam(value = "from_id") int from_id,
                         @RequestParam(value = "to_id") int to_id) {
        distanceService.deleteById(from_id, to_id);
        return "redirect:/destination/edit?id=" + from_id;
    }

    @PostMapping("/save")
    public String save(@Valid @ModelAttribute("distance") Distance distance, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "distance-form";
        }
        distanceService.save(distance);
        return "redirect:/destination/edit?id=" + distance.getFrom().getId();
    }

}
