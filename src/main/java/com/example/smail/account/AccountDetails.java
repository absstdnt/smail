package com.example.smail.account;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

/**
 * @author user
 */
public class AccountDetails implements UserDetails {

    private final Account account;

    AccountDetails(Account account) {
        this.account = account;
    }

    public Account getAccount() {
        return account;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return createAuthorities(account);
    }

    @Override
    public String getPassword() {
        return account.getPassword();
    }

    @Override
    public String getUsername() {
        return account.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    static private GrantedAuthority createAuthority(Account account) {
        return new SimpleGrantedAuthority(account.getRole().name());
    }

    static private Collection<? extends GrantedAuthority> createAuthorities(Account account) {
        return Collections.singleton(createAuthority(account));
    }

    //todo where should I put this method
    public static void signin(Account account) {
        Authentication authentication = new UsernamePasswordAuthenticationToken(account.getEmail(), account.getPassword(), createAuthorities(account));
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    public boolean hasRole(Role role){
        return (account.getRole() == role);
    }
}

