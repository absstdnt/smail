package com.example.smail.account;

/**
 * @author user
 */
public enum Role {

    ROLE_USER("U", "User"), ROLE_AUSER("A", "Authenticated user"), ROLE_MANAGER("M", "Manager");

    final private String code;
    final private String displayValue;

    Role(String code, String displayValue) {
        this.code = code;
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public String getCode() {
        return code;
    }
}