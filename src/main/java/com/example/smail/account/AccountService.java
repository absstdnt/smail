package com.example.smail.account;

import com.example.smail.controller.AllowedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;


@Slf4j
@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class AccountService implements UserDetailsService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    @Transactional
    protected void initialize() {

        final String DEFAULT_ACCOUNT_NAME = "man";

        if (!accountRepository.existsByEmail(DEFAULT_ACCOUNT_NAME)) {
            Account account = Account.builder()
                    .email(DEFAULT_ACCOUNT_NAME)
                    .password(passwordEncoder.encode(DEFAULT_ACCOUNT_NAME))
                    .role(Role.ROLE_MANAGER)
                    .build();

            accountRepository.save(account);

            log.info("Manager account created");
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return new AccountDetails(accountRepository.findOneByEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found: " + username)));
    }

    public Account save(String username, String password) {

        return accountRepository.save(Account.builder()
                .email(username)
                .password(passwordEncoder.encode(password))
                .role(Role.ROLE_USER)
                .build());
    }

    public Object findAll() {
        return accountRepository.findAll();
    }

    public Account findById(int id) {
        return accountRepository.findById(id).orElseThrow(() -> new AllowedException("No account with ID=" + id));
    }

    public void save(Account account) {
        accountRepository.save(account);
    }

    @Transactional
    public void chrole(int id, Role role) {
        accountRepository.chrole(id, role);
    }

}
