package com.example.smail.account;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Controller
@RequestMapping("/account")
public class AccountController {

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/list")
    public String list(Model model) {
        model.addAttribute("accounts", accountService.findAll());
        return "account-list";
    }

    @GetMapping("/edit")
    public String edit(@RequestParam("accountId") int id, Model model) {
        model.addAttribute("account", accountService.findById(id));
        return "/account-form";
    }

    @PostMapping("/chrole")
    public String save(@ModelAttribute("account") Account account) {
        accountService.chrole(account.getId(), account.getRole());
        return "redirect:/account/list";
    }

}
