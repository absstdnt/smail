package com.example.smail.account;

import lombok.*;

import javax.persistence.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Account {

    @Id
    @GeneratedValue
    private int id;

    @Column(unique = true)
    private String email;

    private String password;

    private Role role = Role.ROLE_USER;

}
