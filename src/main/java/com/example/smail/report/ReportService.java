package com.example.smail.report;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;

import java.util.List;
import java.util.Optional;

/**
 * @author user
 */
@Slf4j
@Service
public class ReportService {

    @PersistenceContext
    private EntityManager entityManager;

    public ReportOrderResult getReportOrderResult(Optional<LocalDate> date_start, Optional<LocalDate> date_end, Optional<Integer> from_id, Optional<Integer> to_id) {

        StringBuilder hql = new StringBuilder(
                "SELECT new com.example.smail.report.ReportOrderLine (" +
                " o.number as order_number," +
                " o.created as order_created," +
                " o.deliveryDate as order_deliveryDate," +
                " o.account.email as order_account_email,\n" +
                " o.from.name as order_from_name,\n" +
                " o.to.name as order_to_name)\n" +
                "FROM Order o\n" +
                "WHERE 1=1");

        if (date_start.isPresent())
            hql.append("\nAND o.deliveryDate >= :date_start");

        if (date_end.isPresent())
            hql.append("\nAND o.deliveryDate <= :date_end");

        if (from_id.isPresent())
            hql.append("\nAND o.from.id = :from_id");

        if (to_id.isPresent())
            hql.append("\nAND o.to.id = :to_id");

        Session session = entityManager.unwrap(Session.class);
        Query<ReportOrderLine> query = session.createQuery(hql.toString());
        date_start.ifPresent(d -> query.setParameter("date_start", d));
        date_end.ifPresent(d -> query.setParameter("date_end", d));
        from_id.ifPresent(p -> query.setParameter("from_id", p));
        to_id.ifPresent(p -> query.setParameter("to_id", p));

        List<ReportOrderLine> lines = query.list();

        return new ReportOrderResult(lines);
    }
}
