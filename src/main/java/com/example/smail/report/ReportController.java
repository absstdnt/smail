package com.example.smail.report;

import com.example.smail.destination.DestinationService;
//import com.example.smail.report.Report;
//import com.example.smail.report.ReportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * @author user
 */
@Slf4j
@Controller
@RequestMapping("/report")
public class ReportController {

    @Autowired
    private DestinationService destinationService;

    @Autowired
    private ReportService reportService;

    @GetMapping("/order")
    public String orders(Model model,
                       @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> date_start,
                       @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> date_end,
                       @RequestParam Optional<Integer> from_id,
                       @RequestParam Optional<Integer> to_id) {

        date_start.ifPresent(localDate -> model.addAttribute("date_start", localDate));
        date_end.ifPresent(localDate -> model.addAttribute("date_end", localDate));
        model.addAttribute("from_id", from_id.orElse(-1));
        model.addAttribute("to_id", to_id.orElse(-1));
        model.addAttribute("destinations", destinationService.findAll());
        model.addAttribute("result", reportService.getReportOrderResult(date_start, date_end, from_id, to_id));
        return "report-order";
    }

}
