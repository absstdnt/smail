package com.example.smail.report;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/**
 * @author user
 */
@Getter
@AllArgsConstructor
public class ReportOrderResult {
    private List<ReportOrderLine> lines;
    //private ReportOrderLine total;
}
