package com.example.smail.report;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Entity;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author user
 */
@Slf4j
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ReportOrderLine {
    private int order_number;
    private LocalDateTime order_created;
    private LocalDate order_deliveryDate;
    private String order_account_email;
    private String order_from_name;
    private String order_to_name;
}
