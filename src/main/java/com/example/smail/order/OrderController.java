package com.example.smail.order;

import com.example.smail.account.Account;
import com.example.smail.account.AccountDetails;
import com.example.smail.account.AccountService;
import com.example.smail.destination.DestinationService;
import com.example.smail.invoice.InvoiceService;
import com.example.smail.orderType.OrderTypeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.security.Principal;
import java.time.Instant;

/**
 * @author user
 */
@Slf4j
@Controller
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private DestinationService destinationService;

    @Autowired
    private OrderTypeService orderTypeService;

    @GetMapping("/list")
    public String list(@AuthenticationPrincipal AccountDetails accountDetails,
                       Model model) {
        model.addAttribute("orders", orderService.findAll(accountDetails));
        return "order-list";
    }

    @GetMapping("/add")
    public String add(Model model) {
        model.addAttribute("order", new OrderForm());
        populateModel(model);
        return "order-form";
    }

    @PostMapping("/save")
    public String save(@AuthenticationPrincipal AccountDetails accountDetails,
                       @Valid @ModelAttribute("order") OrderForm orderForm, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            populateModel(model);
            return "order-form";
        }
        orderService.save(orderForm, accountDetails);
        return "redirect:/order/list";
    }

    @PostMapping("/total")
    public String total(@Valid @ModelAttribute("order") OrderForm orderForm, BindingResult bindingResult, Model model) { //, BindingResult bindingResult, Model model
        populateModel(model);
        if (!bindingResult.hasErrors())
            model.addAttribute("total", orderService.getTotal(orderForm).toString());
        return "order-form";
    }

    @GetMapping("/edit")
    public String edit(@AuthenticationPrincipal AccountDetails accountDetails,
                       @RequestParam("id") int id, Model model) {
        Order order = orderService.findById(id, accountDetails);
        OrderForm orderForm = new OrderForm(order);
        model.addAttribute("order", orderForm);
        model.addAttribute("total", order.getTotal());
        model.addAttribute("order_representation", orderService.getRepresentation(order));
        populateModel(model);
        return "order-form";
    }

    @GetMapping("/copy")
    public String copy(@AuthenticationPrincipal AccountDetails accountDetails,
                       @RequestParam("id") int id, Model model) {
        OrderForm orderForm = new OrderForm(orderService.findById(id, accountDetails));
        orderForm.setId(0);
        model.addAttribute("order", orderForm);
        model.addAttribute("order_representation", "Order*");
        populateModel(model);
        return "order-form";
    }

    @GetMapping("/delete")
    public String delete(@RequestParam("id") int id) {
        orderService.deleteById(id);
        return "redirect:/order/list";
    }

    private void populateModel(Model model) {
        model.addAttribute("destinations", destinationService.findAll());
        model.addAttribute("orderTypes", orderTypeService.findAll());
    }

}
