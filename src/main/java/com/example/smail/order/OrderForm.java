package com.example.smail.order;

import com.example.smail.destination.Destination;
import com.example.smail.orderType.OrderType;
import lombok.*;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static com.example.smail.support.web.Messages.FUTURE_DATE;
import static com.example.smail.support.web.Messages.NOT_BLANK_MESSAGE;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderForm {

	private int id;

	@NotNull(message = NOT_BLANK_MESSAGE)
	Destination from;

	@NotNull(message = NOT_BLANK_MESSAGE)
	Destination to;

    @NotBlank(message = NOT_BLANK_MESSAGE)
	//todo
	private String address = "Somewhere...";

	@NotNull(message = NOT_BLANK_MESSAGE)
	private OrderType type;

	@Min(1)
	private int weight;

	@Min(1)
	private int length;

	@Min(1)
	private int width;

	@Min(1)
	private int height;

	@NotNull(message = NOT_BLANK_MESSAGE)
	@FutureOrPresent(message = FUTURE_DATE)
	private LocalDate deliveryDate = LocalDate.now().plusDays(1);

	public OrderForm(Order order) {
		this.id = order.getId();
		this.from = order.getFrom();
		this.to = order.getTo();
		this.address = order.getAddress();
		this.type = order.getType();
		this.weight = order.getWeight();
		this.length = order.getLength();
		this.width = order.getWidth();
		this.height = order.getHeight();
		this.deliveryDate = order.getDeliveryDate();
	}

}
