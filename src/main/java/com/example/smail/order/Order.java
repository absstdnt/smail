package com.example.smail.order;

import com.example.smail.account.Account;
import com.example.smail.destination.Destination;
import com.example.smail.orderType.OrderType;
import lombok.*;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.springframework.context.i18n.LocaleContextHolder;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Locale;

import static com.example.smail.support.web.Messages.*;

/**
 * @author user
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "orders")
public class Order implements Document {
    @ManyToOne
    @JoinColumn(name = "acc_id", updatable = false)
    @NotNull(message = "Field Account cannot be empty")
    Account account;
    @ManyToOne
    @JoinColumn(name = "from_id")
    @NotNull(message = "Field From cannot be empty")
    Destination from;
    @ManyToOne
    @JoinColumn(name = "to_id")
    @NotNull(message = "Field To cannot be empty")
    Destination to;
    @Id
    @GeneratedValue
    private int id;
    @Column(unique = true, updatable = false, columnDefinition = "serial")
    @Generated(GenerationTime.INSERT)
    private int number;
    @Column(updatable = false)
    @NotNull(message = "Field cannot be empty")
    private LocalDateTime created;

    private String address = "Somewhere...";

    @NotNull(message = NOT_BLANK_MESSAGE)
    @ManyToOne
    @JoinColumn(name = "type_id")
    private OrderType type;

    @Min(1)
    private int weight;

    @Min(1)
    private int length;

    @Min(1)
    private int width;

    @Min(1)
    private int height;

    @NotNull(message = NOT_BLANK_MESSAGE)
    @FutureOrPresent(message = FUTURE_DATE)
    private LocalDate deliveryDate;

    @DecimalMin(value = "0.0", inclusive = false, message = GREATER_ZERO_MESSAGE)
    private BigDecimal total;

    public Order(OrderForm orderForm) {
        this.id = orderForm.getId();
        this.from = orderForm.getFrom();
        this.to = orderForm.getTo();
        this.address = orderForm.getAddress();
        this.type = orderForm.getType();
        this.weight = orderForm.getWeight();
        this.length = orderForm.getLength();
        this.width = orderForm.getWidth();
        this.height = orderForm.getHeight();
        this.deliveryDate = orderForm.getDeliveryDate();
    }

    public String getDetails() {
        return new StringBuilder()
                .append(type.getName()).append(' ')
                .append(deliveryDate).append(' ')
                .append(weight).append("kg ")
                .append(length).append("x")
                .append(width).append("x")
                .append(height).append("cm")
                .toString();
    }

}
