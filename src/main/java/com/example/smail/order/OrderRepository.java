package com.example.smail.order;

import com.example.smail.account.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author user
 */
@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {

    List<Order> findAllByAccount(Account account);

    Optional<Order> findByIdAndAccount(int id, Account account);
}
