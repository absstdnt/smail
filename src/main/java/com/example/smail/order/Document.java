package com.example.smail.order;

import java.time.LocalDateTime;

/**
 * @author user
 */
public interface Document {
    int getNumber();
    LocalDateTime getCreated();
}
