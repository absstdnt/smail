package com.example.smail.order;

import com.example.smail.account.Account;
import com.example.smail.account.AccountDetails;
import com.example.smail.account.AccountService;
import com.example.smail.account.Role;
import com.example.smail.controller.AllowedException;
import com.example.smail.controller.DisallowedException;
import com.example.smail.distance.DistanceService;
import com.example.smail.invoice.InvoiceService;
import com.example.smail.price.PriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

import static java.lang.Math.max;

/**
 * @author user
 */
@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private PriceService priceService;

    @Autowired
    private DistanceService distanceService;

    @Autowired
    private MessageSource messageSource;

    public List<Order> findAll(AccountDetails accountDetails) {
        // only Manager is allowed to see all Orders
        if (accountDetails.hasRole(Role.ROLE_MANAGER))
            return orderRepository.findAll();
        return orderRepository.findAllByAccount(accountDetails.getAccount());
    }

    public Order findById(int id, AccountDetails accountDetails) {
        // only Manager is allowed to see all Orders
        return (accountDetails.hasRole(Role.ROLE_MANAGER) ?
                orderRepository.findById(id) :
                orderRepository.findByIdAndAccount(id, accountDetails.getAccount()))
                .orElseThrow(() -> new DisallowedException(
                        "No Order with id=" + id + " " + accountDetails.getUsername(), accountDetails));
    }

    public Order save(OrderForm orderForm, AccountDetails accountDetails) {
        // only Manager is allowed to Edit Orders
        if (orderForm.getId() > 0 && !accountDetails.hasRole(Role.ROLE_MANAGER))
            throw new DisallowedException(
                    "Attempt to edit order " + orderForm.getId() + " " + accountDetails.getUsername(), accountDetails);
        Order order = new Order(orderForm);
        order.setCreated(LocalDateTime.now());
        order.setAccount(accountDetails.getAccount());
        order.setTotal(getTotal(orderForm));
        return orderRepository.save(order);
    }

    public BigDecimal getTotal(OrderForm orderForm) {
        BigDecimal volume = new BigDecimal(orderForm.getWidth() * orderForm.getLength() * orderForm.getHeight())
                .divide(new BigDecimal(1000), RoundingMode.HALF_UP);
        BigDecimal volumeWeight = volume.max(new BigDecimal(orderForm.getWeight()));
        int distance = distanceService.getDistance(orderForm.getFrom(), orderForm.getTo());
        return priceService.getPriceBase()
                .add(priceService.getPriceKmKg()
                        .multiply(new BigDecimal(distance))
                        .multiply(volumeWeight)).setScale(2, RoundingMode.HALF_UP);
    }

    public void deleteById(int id) {
        if (invoiceService.existsByOrderId(id))
            throw new AllowedException("Order " + id + " has Invoices");
        orderRepository.deleteById(id);
    }

    public String getRepresentation(Document document) {

        final Locale locale = LocaleContextHolder.getLocale();
        final LocalDateTime created = document.getCreated();
        final String createdRepresentation;
        final String documentClass = document.getClass().getSimpleName().toLowerCase();

        if (created != null) {
            final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(messageSource.getMessage("representation.dateTime.template", null, locale), locale);
            createdRepresentation = created.format(formatter);
        } else {
            createdRepresentation = "*";
        }

        final int number = document.getNumber();

        return String.format(messageSource.getMessage("representation.document.template", null, locale),
                messageSource.getMessage("entity."+documentClass, null, locale),
                (number > 0 ? number : "*"),
                createdRepresentation);
    }

}
