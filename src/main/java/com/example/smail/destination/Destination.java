package com.example.smail.destination;

import com.example.smail.distance.Distance;
import lombok.*;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

/**
 * @author user
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Destination {
    @Id
    @GeneratedValue
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
//    @Column(unique = true, updatable = false, columnDefinition = "serial")
//    @Generated(GenerationTime.INSERT)
    private int id;

    @Column(unique = true)
    @NotBlank(message = "Name cannot be empty")
    private String name;

    @OneToMany(mappedBy = "from", cascade = CascadeType.ALL)
    private Set<Distance> distances = new HashSet<>();

    @Override
    public String toString() {
        return name;
    }
}

