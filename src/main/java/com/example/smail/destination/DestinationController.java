package com.example.smail.destination;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author user
 */
@Slf4j
@Controller
@RequestMapping("/destination")
public class DestinationController {

    @Autowired
    private DestinationService destinationService;

    @GetMapping("/list")
    public String list(Model model) {
        model.addAttribute("destinations", destinationService.findAll());
        return "destination-list";
    }

    @GetMapping("/add")
    public String add(Model model) {
        model.addAttribute("destination", new Destination());
        return "destination-form";
    }

    @PostMapping("/save")
    public String save(@Valid @ModelAttribute("destination") Destination destination, BindingResult bindingResult) {
        if(bindingResult.hasErrors()){
            return "destination-form";
        }
        destinationService.save(destination);
        return "redirect:/destination/list";
    }

    @GetMapping("/edit")
    public String edit(@RequestParam("id") int id, Model model) {
        model.addAttribute("destination", destinationService.findById(id));
        return "destination-form";
    }

    @GetMapping("/delete")
    public String delete(@RequestParam("id") int id) {
        destinationService.deleteById(id);
        return "redirect:/destination/list";
    }
}
