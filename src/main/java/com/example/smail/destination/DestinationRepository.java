package com.example.smail.destination;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author user
 */
@Repository
public interface DestinationRepository extends JpaRepository<Destination, Integer> {
}
