package com.example.smail.destination;

import com.example.smail.controller.AllowedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author user
 */
@Service
public class DestinationService {

    @Autowired
    private DestinationRepository destinationRepository;

    public List<Destination> findAll() {
        return destinationRepository.findAll();
    }

    public Destination findById(int id) {
        return destinationRepository.findById(id).orElseThrow(() -> new AllowedException("No Destination with id=" + id));
    }

    public void save(Destination destination) {
        destinationRepository.save(destination);
    }

    public void deleteById(int id) {
        destinationRepository.deleteById(id);
    }
}
