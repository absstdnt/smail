package com.example.smail.price;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * @author user
 */
@Service
public class PriceService {
    public BigDecimal getPriceBase(){return new BigDecimal("32.50");}
    public BigDecimal getPriceKmKg(){return new BigDecimal("0.002");}
}
