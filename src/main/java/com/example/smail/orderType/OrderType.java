package com.example.smail.orderType;

import com.example.smail.distance.Distance;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author user
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class OrderType {
    @Id
    @GeneratedValue
    private int id;

    @Column(unique = true)
    @NotBlank(message = "Name cannot be empty")
    private String name;

}
