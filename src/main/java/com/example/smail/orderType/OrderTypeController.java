package com.example.smail.orderType;

import com.example.smail.orderType.OrderType;
import com.example.smail.orderType.OrderTypeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author user
 */
@Slf4j
@Controller
@RequestMapping("/order_type")
public class OrderTypeController {

    @Autowired
    private OrderTypeService orderTypeService;

    @GetMapping("/list")
    public String list(Model model) {
        model.addAttribute("orderTypes", orderTypeService.findAll());
        return "order_type-list";
    }

    @GetMapping("/add")
    public String add(Model model) {
        model.addAttribute("orderType", new OrderType());
        return "order_type-form";
    }

    @PostMapping("/save")
    public String save(@Valid @ModelAttribute("orderType") OrderType orderType, BindingResult bindingResult) {
        if(bindingResult.hasErrors())
            return "order_type-form";
        orderTypeService.save(orderType);
        return "redirect:/order_type/list";
    }

    @GetMapping("/edit")
    public String edit(@RequestParam("id") int id, Model model) {
        OrderType orderType = orderTypeService.findById(id);
        model.addAttribute("orderType", orderType);
        return "order_type-form";
    }

    @GetMapping("/delete")
    public String delete(@RequestParam("id") int id) {
        orderTypeService.deleteById(id);
        return "redirect:/order_type/list";
    }
}
