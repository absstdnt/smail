package com.example.smail.orderType;

import com.example.smail.controller.AllowedException;
import com.example.smail.orderType.OrderType;
import com.example.smail.orderType.OrderTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author user
 */
@Service
public class OrderTypeService {

    @Autowired
    private OrderTypeRepository orderTypeRepository;

    public List<OrderType> findAll() {
        return orderTypeRepository.findAll();
    }

    public OrderType findById(int id) {
        return orderTypeRepository.findById(id).orElseThrow(() -> new AllowedException("No OrderType with id=" + id));
    }

    public OrderType save(OrderType orderType) {
        return orderTypeRepository.save(orderType);
    }

    public void deleteById(int id) {
        orderTypeRepository.deleteById(id);
    }
}
