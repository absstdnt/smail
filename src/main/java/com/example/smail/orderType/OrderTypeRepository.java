package com.example.smail.orderType;

import com.example.smail.destination.Destination;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author user
 */
@Repository
public interface OrderTypeRepository extends JpaRepository<OrderType, Integer> {
}
