package com.example.smail.config;

import com.example.smail.account.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices;

import javax.sql.DataSource;

@Slf4j
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

//    @Autowired
//    private DataSource dataSource;

    @Autowired
    private AccountService accountService;

//    @Bean
//    public TokenBasedRememberMeServices rememberMeServices() {
//        return new TokenBasedRememberMeServices("remember-me-key", accountService);
//    }

    @Bean("authenticationManager")
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/",
                        "/home",
                        "/about",
                        "/contact",
                        "/css/**",
                        "/fonts/**",
                        "/js/**",
                        "/signup").permitAll()
                .antMatchers("/order/total",
                        "/order/add",
                        "/distance/list").hasAnyRole("USER", "AUSER", "MANAGER")
                .antMatchers("/order/list",
                        "/order/save",
                        "/order/edit",
                        "/invoice/list").hasAnyRole("AUSER", "MANAGER")
                .antMatchers(
                        "/invoice/pay").hasRole("AUSER")
                .antMatchers("/account/**",
                        "/order/**",
                        "/orderType/**",
                        "/distance/**",
                        "/destination/**",
                        "/invoice/**",
                        "/report/**").hasRole("MANAGER")
//                    .antMatchers("/",
//                            "/home",
//                            "/about",
//                            "/contact",
//                            "/distance/list",
//                            "/destination/list",
//                            "/css/**",
//                            "/fonts/**",
//                            "/js/**",
//                            "/signup").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/signin")
                //.defaultSuccessUrl("/", true)
                .loginProcessingUrl("/authenticate")
                .permitAll()
                .and()
                .logout().deleteCookies("JSESSIONID")
                .permitAll()
                .and()
                .rememberMe()
//                    .rememberMeServices(rememberMeServices())
                .key("remember-mhfcjcgfchgfce-key");


//                    .loginPage("/signin")
//                    .permitAll()
//                    .failureUrl("/signin?error=1")
//                    .loginProcessingUrl("/authenticate")
//                    .and()
//                .logout()
//                    .logoutUrl("/logout")
//                    .permitAll()
//                    .logoutSuccessUrl("/signin?logout")
//                .and()
//                    .rememberMe()
//                    .rememberMeServices(rememberMeServices())
//                    .key("remember-me-key");
    }

//    @Bean
//    @Override
//    public UserDetailsService userDetailsService() {
//        UserDetails user =
//                User.withDefaultPasswordEncoder()
//                        .username("user")
//                        .password("user")
//                        .roles("USER")
//                        .build();
//
//        return new InMemoryUserDetailsManager(user);
//
//    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

//        // add our users for in memory authentication
//        User.UserBuilder users = User.withDefaultPasswordEncoder();
//
//        auth.inMemoryAuthentication()
//                .withUser(users.username("user").password("user").roles("USER"))
//                .withUser(users.username("auser").password("auser").roles("AUSER"))
//                .withUser(users.username("admin").password("admin").roles("ADMIN"));

//        auth.jdbcAuthentication().dataSource(dataSource);
        auth
                .eraseCredentials(true)
                .userDetailsService(accountService)
                .passwordEncoder(passwordEncoder());
    }

//    @Bean
//    public UserDetailsManager userDetailsManager() {
//        //return new JdbcUserDetailsManager(dataSource);
//        return new AccountService();
//    }
}
