package com.example.smail.invoice;

import com.example.smail.account.AccountDetails;
import com.example.smail.order.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author user
 */
@Slf4j
@Controller
@RequestMapping("/invoice")
public class InvoiceController {

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private OrderService orderService;

    @GetMapping("/list")
    public String list(@AuthenticationPrincipal AccountDetails accountDetails,
                       Model model) {
        model.addAttribute("invoices", invoiceService.findAll(accountDetails));
        return "invoice-list";
    }

    @GetMapping("/add")
    public String add(@AuthenticationPrincipal AccountDetails accountDetails,
                      @RequestParam("order_id") int order_id, Model model) {
        model.addAttribute("invoice", new Invoice(orderService.findById(order_id, accountDetails)));
        return "invoice-form";
    }

    @PostMapping("/save")
    public String save(@Valid @ModelAttribute("invoice") Invoice invoice, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "invoice-form";
        }
        invoiceService.save(invoice);
        return "redirect:/invoice/list";
    }

    @GetMapping("/edit")
    public String edit(@AuthenticationPrincipal AccountDetails accountDetails,
                       @RequestParam("id") int id, Model model) {
        Invoice invoice = invoiceService.findById(id, accountDetails);
        model.addAttribute("invoice", invoice);
        return "invoice-form";
    }

    @GetMapping("/delete")
    public String delete(@RequestParam("id") int id) {
        invoiceService.deleteById(id);
        return "redirect:/invoice/list";
    }

    @GetMapping("/pay")
    public String pay(@AuthenticationPrincipal AccountDetails accountDetails,
                      @RequestParam("id") int id) {
        invoiceService.payById(id, accountDetails);
        return "redirect:/invoice/list";
    }

}
