package com.example.smail.invoice;

import com.example.smail.account.Account;
import com.example.smail.order.Document;
import com.example.smail.order.Order;
import lombok.*;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import static com.example.smail.support.web.Messages.*;

/**
 * @author user
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Invoice implements Document {

    @Id
    @GeneratedValue
    private int id;

    @ManyToOne
    @JoinColumn(name = "acc_id", updatable = false)
    //@NotNull(message = "Field Account cannot be empty")
    Account account;


    @Column(unique = true, updatable = false, columnDefinition = "serial")
    @Generated(GenerationTime.INSERT)
    private int number;

    @Column(updatable = false)
    //@NotNull(message = "Field cannot be empty")
    private LocalDateTime created;

    @NotNull(message = NOT_BLANK_MESSAGE)
    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;

    @DecimalMin(value = "0.0", inclusive = false, message = GREATER_ZERO_MESSAGE)
    private BigDecimal total;

    @Column(columnDefinition = "boolean default false")
    private boolean paid;

    private String comment;

    public Invoice(Order order) {
        this.order = order;
        this.total = order.getTotal();
        this.comment = "Invoice of the order #" + order.getNumber();
    }

//    public String getRepresentation() {
//        if ()
//        StringBuilder sb = new StringBuilder("Invoice ");
//        sb.append(number == 0 ? "*" : number);
//        sb.append(" from ");
//        sb.append(created == null ? '*' : created.toString());
//        return sb.toString();
//    }
}
