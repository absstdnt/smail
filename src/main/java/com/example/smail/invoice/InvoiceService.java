package com.example.smail.invoice;

import com.example.smail.account.AccountDetails;
import com.example.smail.account.Role;
import com.example.smail.controller.DisallowedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author user
 */
@Service
public class InvoiceService {

    @Autowired
    private InvoiceRepository invoiceRepository;

    public List<Invoice> findAll(AccountDetails accountDetails) {
        // only Manager is allowed to see all Orders
        if (accountDetails.hasRole(Role.ROLE_MANAGER))
            return invoiceRepository.findAll();
        return invoiceRepository.findAllByAccount(accountDetails.getAccount());
    }

    public Invoice findById(int id, AccountDetails accountDetails) {
        return (accountDetails.hasRole(Role.ROLE_MANAGER) ?
                    invoiceRepository.findById(id) :
                    invoiceRepository.findByIdAndAccount(id, accountDetails.getAccount()))
                .orElseThrow(() -> new DisallowedException(
                        "No Invoice with id=" + id + " " + accountDetails.getUsername(), accountDetails));
    }

    public void save(Invoice invoice) {
        invoice.setCreated(LocalDateTime.now());
        invoice.setAccount(invoice.getOrder().getAccount());
        invoiceRepository.save(invoice);
    }

    public void deleteById(int id) {
        invoiceRepository.deleteById(id);
    }

    @Transactional
    public void payById(int id, AccountDetails accountDetails) {
        if(!invoiceRepository.existsByIdAndAccount(id, accountDetails.getAccount()))
            throw new DisallowedException(
                    "No Invoice to pay with id=" + id + " " + accountDetails.getUsername(), accountDetails);
        invoiceRepository.setPaidByIdAndAccount(id, accountDetails.getAccount());
    }

    public boolean existsByOrderId(int id) {
        return invoiceRepository.existsByOrderId(id);
    }
}
