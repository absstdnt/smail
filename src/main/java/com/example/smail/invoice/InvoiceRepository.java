package com.example.smail.invoice;

import com.example.smail.account.Account;
import com.example.smail.order.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author user
 */
@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Integer> {

    List<Invoice> findAllByAccount(Account account);

    Optional<Invoice> findByIdAndAccount(int id, Account account);

    boolean existsByOrderId(int id);

    boolean existsByIdAndAccount(int id, Account account);

    @Modifying(flushAutomatically = true)
    @Query("update Invoice set paid = true where id = :id and account = :account")
    void setPaidByIdAndAccount(@Param("id") int id, @Param("account") Account account);
}
